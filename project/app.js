/**
 * Created by Анна on 26.03.2016.
 */


var express = require("express");
var sqlite = require('sqlite3');
var app = express();
app.set('view engine', 'ejs');
var db = new sqlite.Database('mydb.sqlite');

app.get('/', function (req, res) {

    db.all('select * from mydata', function(error, data) {
        res.render('pic', {
            data : data
        });
    })
});

app.post('/new', function (req, res) {
    db.run("insert into mydata (text) values('"+req.query.text+"')", function() {
        db.all('SELECT MAX(id) as maximum FROM mydata', function(error, data) {
            data.forEach(function (row) {
                var maximum = row.maximum;
                console.log(maximum);
                res.send('ok' + maximum);
            });

        });
    })
});

app.post('/delete', function (req, res) {
    db.run("delete from mydata where id = "+req.query.id, function() {
        res.send('ok');
    })
});

app.listen(3000, function () {
    console.log('listening on 3000 port\n\n');
});
